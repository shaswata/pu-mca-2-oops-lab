import java.applet.*;
import java.awt.*;

public class AnimateFun extends Applet implements Runnable {

    private static final long serialVersionUID = 01L;

    Image picture;
    Image mario;
    int mario_x = 0;

    Thread anime;

    public void init() {
        picture = getImage(getDocumentBase(),"Fireworks.gif");
        mario = getImage(getDocumentBase(),"mario.gif");
        setBackground(Color.WHITE);
    }

    public void start() {
        if(anime == null) {
            anime = new Thread(this);
            anime.start();
        }
    }

    public void stop() {
        if(anime != null) {
            anime.interrupt();
            anime = null;
        }
    }

    public void run() {
        while(true) {
            mario_x = (mario_x + 10) % 500;
            repaint();
            try {
                Thread.sleep(100);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void paint(Graphics g) {
        g.drawImage(picture, 100,100, this);
        g.drawImage(mario, mario_x, 30, this);
    }
}
/*
<applet code="AnimateFun" height="500" width="500"></applet>
*/
