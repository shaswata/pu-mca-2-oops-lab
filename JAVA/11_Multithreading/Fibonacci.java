class Fibonacci extends Thread implements MaxLimit {
    int num[] = new int[MAX_LIMIT];
    Fibonacci(String n) {
        super(n);
        for (int i = 0; i < MAX_LIMIT; i++) {
            num[i] = -1;
        }
    }
    public void run() {
        int f1 = -1, f2 = 1, f3 = 0, k = 0;
        while (f3 <= MAX_LIMIT) {
            f3 = f1 + f2;
            num[k] = f3;
            k++;
            f1 = f2;
            f2 = f3;
        }
    }
}
