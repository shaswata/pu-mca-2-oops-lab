class Prime extends Thread implements MaxLimit {
    int number[] = new int[MAX_LIMIT];
    Prime(String n) {
        super(n);
        for (int i = 0; i < MAX_LIMIT; i++) {
            number[i] = -1;
        }
    }
    public void run() {
        int k = 0, flag;
        for (int i = 2; i <= MAX_LIMIT; i++) {
            flag = 0;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                number[k] = i;
                k++;
            }
        }
    }
}
