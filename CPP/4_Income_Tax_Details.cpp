/**
* Display Income Tax Details
*/
#include <iostream>
#include <cstdlib>

using namespace std;

float sal, it_calc, it = 0;
void getdata(); //GLOBAL DECLARATION

void getdata() {
    void calc(); // LOCAL DECLARATION
    cout << "INCOME TAX CALCULATION" << endl << endl;
    cout << "Enter the salary : ";
    cin >> sal;
    calc();
}

void calc() {
    if( sal <= 100000 )
        it = 0;
    else if( sal <=200000 ) {
        it_calc=sal-100000;
        it=it_calc * .02;
    } else if( sal <= 300000 ) {
        it_calc = sal - 200000;
        it = ( it_calc * 0.025 ) + ( 100000 * 0.02 );
    } else {
        it_calc = sal - 300000;
        it = ( it_calc * 0.030 ) + ( 100000 * 0.025 ) + ( 100000 * 0.020 );
    }
    void sprint();
    sprint();
    // BLOCK DECLARATION
}

void sprint() {
    cout << "\n\nThe corresponding IT for salary amount " << sal << " is " << it << endl;
}

int main() {
    getdata();
    return EXIT_SUCCESS;
}
