/**
* A Program to find the Inverse of a 3 X 3 Matrix
*/
#include <iostream>
#include <cstdlib>

using namespace std;

int main() {
    int mat[3][3], i, j;
    float val1, val2, val3;
    float det = 0;

    cout << "INVERSE OF A MATRIX \n------------------------" << endl;
    cout << "Enter elements of 3x3 matrix:" << endl;

    for( i = 0; i < 3; i++ ) {
        for( j = 0; j < 3; j++ )
            cin >> mat[i][j];
    }

    cout << "\nThe entered matrix is:" << endl;

    for( i = 0; i < 3; i++ ) {
        for( j = 0; j < 3; j++ )
            cout << mat[i][j] << " ";
        cout << endl;
    }

    for( i = 0; i < 3; i++ )
        det += ( mat[0][i] * ( mat[1][(i+1)%3] * mat[2][(i+2)%3] -
                            mat[1][(i+2)%3] * mat[2][(i+1)%3]) );

    cout << "Determinant = " << det << endl;

    if( det == 0 )
        cout << "Inverse does not exist (det=0)." << endl;

    else {
        cout << "\nInverse of matrix is: " << endl;
        for( j = 0; j < 3; j++ ) {
            for( i = 0; i < 3; i++ ) {
                val1 = mat[(i+1)%3][(j+1)%3] * mat[(i+2)%3][(j+2)%3];
                val2 = mat[(i+1)%3][(j+2)%3] * mat[(i+2)%3][(j+1)%3];
                val3 = (val1-val2)/det;
                if( val3 == 0 )
                    cout << 0 << "\t"; // to avoid print -0
                else
                    cout << val3 << "\t";
            }
            cout << endl;
        }
    }

    return EXIT_SUCCESS;
}
