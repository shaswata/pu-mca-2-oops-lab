/**
* Binary to Gray Code Conversion and vice versa
*/
#include <iostream>
#include <cmath>
#include <cstring>
#include <cstdlib>

using namespace std;

int binToGray(int bin) {
    int a, b, result = 0, i = 0;
    while ( bin != 0 ) {
        a = bin % 10;
        bin = bin / 10;
        b = bin % 10;
        if ((a && !b) || (!a && b))
            result = result + pow(10, i);
        i++;
    }
    return result;
}

void grayToBin() {
    char gray[8], binary[8];
    cout << "Enter the gray code ";
    cin >> gray;
    binary[0] = gray[0];
    int i = 0;
    for ( i = 0; i < strlen(gray) - 1; i++) {
        if (binary[i] == gray[i + 1])
            binary[i + 1] = '0';
        else
            binary[i + 1] = '1';
    }
    cout << "BINARY VALUE FOR ENTERED GRAY CODE IS " << binary << endl;
}

int main() {
    int gray, bin, ch;

    cout << "BINARY TO GRAY AND BACK" << endl;
    cout << "MENU\n1.BINARY to GRAY conversion\n2.GRAY to BINARY conversion" << endl;
    cin >> ch;
    switch(ch) {
        case 1:
            cout << "Enter the binary value ";
            cin >> bin;
            gray = binToGray(bin);
            cout << "GRAY CODE FOR THE ENTERED BINARY VALUE IS " << gray << endl;
            break;
        case 2:
            grayToBin();
            break;
    }
    return EXIT_SUCCESS;
}
