/**
* Student details using structure
*/
#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

struct marks {
    int m1, m2, m3;
    float total, avg;
    string grade;
};

struct stu {
    int rno, age;
    struct marks mrk[50];
    char name[50], gender;
} s[50];

void getdata(int n) {
    int i;
    for( i = 0; i < n; i++ ) {
        cout << "\nEnter the rollnumber of " << i+1 << ":\t" ;
        cin >> s[i].rno;
        cout << "\nEnter the name of " << i+1 << ":\t" ;
        cin >> s[i].name;
        cout << "\nEnter the gender(M/F) of " << i + 1 << ":\t";
        cin >> s[i].gender;
        cout << "\nEnter the age of " << i + 1 << ":\t";
        cin >> s[i].age;
        cout << "\nEnter the 3 subjects marks of student " << i + 1 << ":\t";
        cin >> s[i].mrk[i].m1 >> s[i].mrk[i].m2 >> s[i].mrk[i].m3;
        s[i].mrk[i].total = s[i].mrk[i].m1 + s[i].mrk[i].m2 + s[i].mrk[i].m3;
        s[i].mrk[i].avg = s[i].mrk[i].total / 3;
    }
}

void calc(int n) {
    int i;
    for(i=0;i<n;i++) {
        if( s[i].mrk[i].avg > 88 && s[i].mrk[i].avg <= 100 )
            s[i].mrk[i].grade="O+";
        else if( s[i].mrk[i].avg >= 83 && s[i].mrk[i].avg <= 88 )
            s[i].mrk[i].grade = "A+";
        else if( s[i].mrk[i].avg >= 76 && s[i].mrk[i].avg <= 82 )
            s[i].mrk[i].grade = "A";
        else if( s[i].mrk[i].avg >= 70 && s[i].mrk[i].avg <= 75 )
            s[i].mrk[i].grade = "A-";
        else if( s[i].mrk[i].avg >= 64 && s[i].mrk[i].avg <= 69 )
            s[i].mrk[i].grade = "B+";
        else if( s[i].mrk[i].avg >= 57 && s[i].mrk[i].avg <= 63 )
            s[i].mrk[i].grade = "B";
        else if( s[i].mrk[i].avg >= 50 && s[i].mrk[i].avg <= 56 )
            s[i].mrk[i].grade = "C";
        else
            s[i].mrk[i].grade = "F";
    }
    cout << "\n" << "ROLLNUMBER\t NAME\t GENDER\t AGE\t AVG\t TOTAL\t GRADE" << endl;
    for(i=0;i<n;i++) {
        cout<<s[i].rno<<"\t"<<s[i].name<<"\t"<<s[i].gender<<"\t"<<s[i].age<<"\t"<<s[i].mrk[i].avg<<"\t"<<s[i].
        mrk[i].total<<"\t"<<s[i].mrk[i].grade;
        cout<<"\n";
    }
}

int main() {
    int n;
    cout << "Enter the number of students : ";
    cin >> n;
    getdata(n);
    calc(n);
    return EXIT_SUCCESS;
}
