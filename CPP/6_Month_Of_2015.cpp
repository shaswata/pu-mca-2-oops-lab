/**
* Display calender of the month of 2015
*/

#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

string months[] = {
    "",
    "\nJanuary",
    "\nFebruary",
    "\nMarch",
    "\nApril",
    "\nMay",
    "\nJune",
    "\nJuly",
    "\nAugust",
    "\nSeptember",
    "\nOctober",
    "\nNovember",
    "\nDecember"
};

int month[] = {0, 31, 28, 31, 30, 31, 30, 31 ,31 ,30, 31, 30, 31};

void cal(int mo, int yr, int fday) {
    int i;
    cout << "\n" << months[mo] << " " << yr << endl << endl;
    cout<<"Sun Mon Tue Wed Thu Fri Sat\n";
    for(i = 0; i <= fday; i++)
        cout << "  ";

    for(i =1; i <= month[mo]; i++) {
        if(i<10)
            cout << "0" << i;
        else
            cout << i;
        if((i + fday)%7 > 0)
            cout << "  ";
        else
            cout << endl;
    }
}

int main() {
    int y=2015;
    int m;
    int fday;
    do{
        cout << "Enter a month (1 - 12): ";
        cin >> m;
        cout << "Enter the first day (Mon-1,Tue-2)";
        cin >> fday;
    } while ((m < 1 || m > 12) && (fday <1 || fday >7));
    cal(m,y,fday);
    cout << endl;
    return EXIT_SUCCESS;
}
