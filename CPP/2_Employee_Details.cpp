/**
* Program to Calculate Salary of Employee
*/
#include <iostream>
#include <cstdlib>

using namespace std;

typedef struct Employee {
    char emp_name[20], emp_num[10];
    float hra, da, net, basic, ta, med, it, pf, gross;

    void get_det() {
        cout << "Enter the name : \t";
        cin >> emp_name;
        cout << "\nEnter the emp num : \t";
        cin >> emp_num;
        cout << "\nEnter the basic pay : \t";
        cin >> basic;
    }

    void calc() {
        hra = 0.05 * basic; da = 0.025 * basic;
        it = 0.010 * basic; pf = 0.02 * basic;
        med= 0.015 * basic; ta = 0.022 * basic;
        net = basic + hra + da + ta + med;
        gross = net - pf - it;
    }

    void show() {
        cout << "\nEmployee " << emp_name << "'s salary details";
        cout << "\n Basic Pay : " << basic << "\n HRA : " << hra << "\n DA : " << da;
        cout << "\n IT\t : " << it << "\n PF : " << pf;
        cout << "\n TA\t : " << ta << "\n MEDICAL ALLOWANCE : " << med;
        cout << "\n NET PAY\t : " << net << "\n GROSS PAY : " << gross << "\n";
    }
} Employee;

int main() {
    Employee emp[2];

    for(int i = 0; i < 2; i++) {
        emp[i].get_det();
        emp[i].calc();
    }

    for(int i = 0; i < 2; i++)
        emp[i].show();

    return EXIT_SUCCESS;
}
