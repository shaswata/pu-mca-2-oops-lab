/**
* Implement functions of a scientific calculator
*/
#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;

int main () {
    char letter;
    char letter1;
    char letter2;
    char letter3;
    char letter4;
    int a,b;
    double a1,b1;
    int result;
    double result1;
    cout<<"***************** SCIENTIFIC CALCULATOR ******************" << endl << endl;
    do {
        cout<<"1 : Arithmetic Operations" << endl;
        cout<<"2 : Trigonometric Functions" << endl;
        cout<<"3 : Logarithmic Functions" << endl;
        cout<<"4 : Power Functions" << endl;
        cout<<"5 : Exit... " <<  endl;
        cin >> letter;
        switch(letter) {
            case '1':
                cout << endl << endl;
                cout << "	1 : Addition" << endl;
                cout << "	2 : Subtraction" << endl;
                cout << "	3 : Multipilication" << endl;
                cout << "	4 : Division" << endl << endl;
                cin >> letter1;
                switch(letter1) {
                    case '1':
                        cout << "Enter first number...";
                        cin >> a;
                        cout << "Enter an other number...";
                        cin >> b;
                        result = a + b;
                        cout << "Result = " << result << endl;
                        break;
                    case '2':
                        cout << "Enter first number...";
                        cin >> a;
                        cout << "Enter an other number...";
                        cin >> b;
                        result = a - b;
                        cout << "Result = " << result << endl;
                        break;
                    case '3':
                        cout << "Enter first number...";
                        cin >> a;
                        cout << "Enter an other number...";
                        cin >> b;
                        result = a * b;
                        cout << "Result = " << result << endl;
                        break;
                    case '4':
                        cout << "Enter first number...";
                        cin >> a;
                        cout << "Enter an other number...";
                        cin >> b;
                        if( a != 0 ) {
                            result=a/b;
                            cout << "Result = " << result << endl;
                        }
                        break;
                }// end of inner switch
                break;// end of case 1 arithmatic operation
            case '2':
                cout << endl << endl;
                cout<<"	1 : Sin function" << endl;
                cout<<"	2 : Cos function" << endl;
                cout<<"	3 : Tan function" << endl;
                cin >> letter2;
                switch(letter2) {
                    case '1':
                        cout << "Enter a number...";
                        cin >> a1;
                        result1 = ( sin(a1) );
                        cout << "Result = " << result1 << endl;
                        break;
                    case '2':
                        cout << "Enter a number...";
                        cin >> a1;
                        result1 = (cos(a1));
                        cout << "Result = " << result1 << endl;
                        break;
                    case '3':
                        cout << "Enter a number...";
                        cin >> a1;
                        result1 = (tan(a1));
                        cout << "Result = " << result1 << endl;
                        break;
                }// inner switch
                break;//inner case 2 trignomatic
            case '3':
                cout << endl << endl;
                cout<<"	1 : Natural log" << endl;
                cout<<"	2 : log with base 10" << endl;
                cin >> letter3;
                switch(letter3) {
                    case '1':
                        cout << "Enter a number...";
                        cin >> a1;
                        result1 = log(a1);
                        cout << "Result = " << result1 << endl;
                        break;
                    case '2':
                        cout << "Enter a number...";
                        cin >> a1;
                        result1 = log10(a1);
                        cout << "Result = " << result1 << endl;
                        break;
                }// end of switch
                break;// end of case 3 logrithmic
            case '4':
                system("clear");
                cout<<"1) Press 1 for Power";
                cout<<"2) Press 2 for Square root";
                cout<<"Enter your choice....";
                cin >> letter4;
                switch(letter4) {
                    case '1':
                        cout<<"Enter a number...";
                        cin>>a1;
                        cout<<"Enter power...";
                        cin>>b1;
                        result1=pow(a1,b1);
                        cout<<"Result = "<<result1<<endl;
                        break;
                    case '2':
                        cout << "Enter a number...";
                        cin >> a;
                        result1 = sqrt(a);
                        cout << "Result = " << result1 << endl;
                        break;
                }// end of switch
                break; // end of case power function
            }// outer switch
    }while(letter != '5');

    return EXIT_SUCCESS;
}
