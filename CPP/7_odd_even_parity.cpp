/**
* Check odd / even parity
*/

#include <iostream>
#include <cstdlib>

using namespace std;

int main() {
    int binary, count_of_ones = 0;
    cout << "Enter Binary Number To Check Parity : ";
    cin >> binary;

    while(binary != 0) {
        if(binary % 10 == 1)
            count_of_ones++;
        binary /= 10;
    }

    if(count_of_ones & 1)
        cout << "ODD PARITY";
    else
        cout << "EVEN PARITY";
    cout << endl;

    return EXIT_SUCCESS;
}
